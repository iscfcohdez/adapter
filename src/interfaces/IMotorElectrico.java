package interfaces;

public interface IMotorElectrico {
	
	public void conectar();
	public void activar();
	public void moverMasRapido();
	public void detener();
	public void desconectar();
	public void recorrido();

}
