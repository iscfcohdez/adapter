package interfaces;

public interface IMotorGasolina {
	
	public void encender();
	public void acelerar();
	public void apagar();
	public void recorrido();

}
