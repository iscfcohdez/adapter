package automoviles;

import interfaces.IMotorGasolina;

public abstract class Automovil {
	
	IMotorGasolina motorGasolina;
		
	public void asignarMotorGasolina(IMotorGasolina motorGasolina) {
		
		this.motorGasolina=motorGasolina;
		
	};
	
	public abstract void recorrido(); 
	
	
}
