package automoviles;

import motores.MotorElectricoAdapter;

public class Delorean extends Automovil {

	MotorElectricoAdapter motorElectricoAdapter;
	boolean ambosMotores=false;
	
	public void usarMotorElectrico() {
		
		motorElectricoAdapter.recorrido();
		
		
	}
	
	public void usarAmbosMotores() {
		
		ambosMotores=true;
		recorrido();
	}
	
	
	public void asignarMotorElectricoAdapter(MotorElectricoAdapter motorElectricoAdapter) {
		this.motorElectricoAdapter= motorElectricoAdapter;
	}
	
	public void recorrido() {
		
		if(ambosMotores) {
			
			System.out.println("Usando Motor Electrico...");
			motorElectricoAdapter.recorrido();
			System.out.println("Bateria agotada...Usando motor de Gasolina");
			motorGasolina.recorrido();			
		
		}else {
			
			if(motorElectricoAdapter==null) {
				motorGasolina.recorrido();
			}else {
				motorElectricoAdapter.recorrido();
			}
		}
	}
	
}
