package motores;

import interfaces.IMotorGasolina;

public class MotorEconomico implements IMotorGasolina {

	@Override
	public void encender() {
		
		System.out.println("Motor Economico encendido...");
	}

	@Override
	public void acelerar() {
		System.out.println("Motor Economico acelerando..");
	}

	@Override
	public void apagar() {
		System.out.println("Motor Economico apagado...");
	}

	@Override
	public void recorrido() {
		encender();
		acelerar();
		apagar();
	}
}
