package motores;

import interfaces.IMotorGasolina;

public class MotorComun implements IMotorGasolina {

	@Override
	public void encender() {
		
		System.out.println("Motor Comun encendido...");
	}

	@Override
	public void acelerar() {
		System.out.println("Motor Comun acelerando..");
	}

	@Override
	public void apagar() {
		System.out.println("Motor Comun apagado...");
	}

	@Override
	public void recorrido() {
		encender();
		acelerar();
		apagar();
	}
	
}
