package motores;

import interfaces.IMotorElectrico;

public class MotorElectrico implements IMotorElectrico {

	@Override
	public void conectar() {
		System.out.println("Motor Electrico conectado...");
	}

	@Override
	public void activar() {
		System.out.println("Motor Electrico activado...");
	}

	@Override
	public void moverMasRapido() {
		System.out.println("Motor Electrico moviendose mas rapido...");
	}

	@Override
	public void detener() {
		System.out.println("Motor Electrico detenido...");
	}

	@Override
	public void desconectar() {
		System.out.println("Motor Electrico desconectado...");
	}

	@Override
	public void recorrido() {
		conectar();
		activar();
		moverMasRapido();
		detener();
		desconectar();
	}
}
