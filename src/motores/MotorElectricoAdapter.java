package motores;

import interfaces.IMotorElectrico;
import interfaces.IMotorGasolina;

public class MotorElectricoAdapter implements IMotorGasolina {

	IMotorElectrico motorElectrico;
	
	public MotorElectricoAdapter(IMotorElectrico motorElectrico) {
		
		this.motorElectrico= motorElectrico;
		
	}
	
	@Override
	public void encender() {
		
		motorElectrico.conectar();
		motorElectrico.activar();
	
	}

	@Override
	public void acelerar() {
		
		motorElectrico.moverMasRapido();
		
	}

	@Override
	public void apagar() {
		
		motorElectrico.detener();
		motorElectrico.desconectar();
		
	}

	@Override
	public void recorrido() {
		encender();
		acelerar();
		apagar();
	}

	
}
