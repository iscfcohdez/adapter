package app;

import automoviles.Delorean;
import motores.MotorComun;
import motores.MotorEconomico;
import motores.MotorElectrico;
import motores.MotorElectricoAdapter;

public class AdapterApp {

	public static void main(String[] args) {
		
		Delorean delorean = new Delorean();
		MotorComun motorComun = new MotorComun();
		MotorEconomico motorEconomico = new MotorEconomico();
		MotorElectrico motorElectrico = new MotorElectrico();
		MotorElectricoAdapter motorElectricoAdapter = new MotorElectricoAdapter(motorElectrico);
		
		System.out.println("Configurando Automovil con Motor Comun");
		delorean.asignarMotorGasolina(motorComun);
		delorean.recorrido();
		System.out.println("");
		
		System.out.println("Configurando Automovil con Motor Economico");
		delorean.asignarMotorGasolina(motorEconomico);
		delorean.recorrido();
		System.out.println("");
		
		System.out.println("Configurando Automovil con Motor Electrico");
		delorean.asignarMotorElectricoAdapter(motorElectricoAdapter);
		delorean.recorrido();
		System.out.println("");
		
		System.out.println("Configurando Automovil para el uso de Ambos motores");
		delorean.usarAmbosMotores();
		

	}

}
